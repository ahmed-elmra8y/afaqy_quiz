- Server Requirements
    PHP >= 7.3
    BCMath PHP Extension
    Ctype PHP Extension
    Fileinfo PHP Extension
    JSON PHP Extension
    Mbstring PHP Extension
    OpenSSL PHP Extension
    PDO PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    
- How to install and use api
    install and setup laravel framework
    after installing laravel u can clone from 
    git clone https://ahmed-elmra8y@bitbucket.org/ahmed-elmra8y/afaqy_quiz.git
    after clone run composer install in project directory 
    then add .env file and add database name , user name and password
    now run project via php artisan serve 
    after that you can access api via this route api/vehicle_expenses "post request"
    you can do a search , filter , sort by send parameters  via post man or any other way
    parameter for search "name" 
    parameter for sort "sortByCost" , "sortByCreationDate" , "sortByDesc"
    parameter for filter "minimumCost" , "maximumCost"  , "minimumCreationDate"  , "maximumCreationDate" 

        
