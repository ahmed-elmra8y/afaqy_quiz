<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Models\FuelEntry;
use App\Models\InsurancePayment;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleExpensesController extends Controller
{
    /*
     * keys used to filter and sort
        name
        minimumCost
        maximumCost
        minimumCreationDate
        maximumCreationDate
        sortByCost
        sortByCreationDate
        sortByDesc
     */
    public function services()
    {
        return Service::query()
            ->join('vehicles', 'vehicles.id', '=', 'services.vehicle_id')
            ->select([
                "vehicles.id",
                "vehicles.name as vehicleName",
                "vehicles.plate_number as plateNumber",
                "services.total as cost",
                DB::raw('DATE_FORMAT(services.created_at,"%Y-%m-%d") as createdAt'),
                DB::raw("'service' as type ")
            ]);
    }

    public function fuels()
    {
        return FuelEntry::query()
            ->join('vehicles', 'vehicles.id', '=', 'fuel_entries.vehicle_id')
            ->select([
                "vehicles.id",
                "vehicles.name as vehicleName",
                "vehicles.plate_number as plateNumber",
                "fuel_entries.cost",
                DB::raw('DATE_FORMAT(fuel_entries.entry_date ,"%Y-%m-%d") as createdAt'),
                DB::raw("'fuel' as type")
            ]);
    }

    public function insurances()
    {
        return InsurancePayment::query()
            ->join('vehicles', 'vehicles.id', '=', 'insurance_payments.vehicle_id')
            ->select([
                "vehicles.id",
                "vehicles.name as vehicleName",
                "vehicles.plate_number as plateNumber",
                "insurance_payments.amount as cost",
                DB::raw('DATE_FORMAT(insurance_payments.contract_date,"%Y-%m-%d") as createdAt'),
                DB::raw("'insurances' as type")
            ]);
    }

    public function searchByName($request, $query)
    {
        if ($request->name)
            $query->where('vehicleName', 'like', '%' . $request->name . '%');
        return $query;
    }

    public function filterResults($request, $query)
    {
        if ($request->minimumCost) {
            $query = $query->where('cost', '>=', $request->minimumCost);
        }

        if ($request->maximumCost) {
            $query = $query->where('cost', '<=', $request->maximumCost);
        }

        if ($request->minimumCreationDate) {
            $query = $query->where('createdAt', '>=', $request->minimumCreationDate);
        }

        if ($request->maximumCreationDate) {
            $query = $query->where('createdAt', '<=', $request->maximumCreationDate);
        }

        return $query;
    }

    public function sortResults($request, $query)
    {
        if ($request->sortByCost) {
            if ($request->sortByDesc)
                $query = $query->orderByDesc('cost');
            else
                $query = $query->orderBy('cost');
        }

        if ($request->sortByCreationDate) {
            if ($request->sortByDesc)
                $query = $query->orderByDesc('createdAt');
            else
                $query = $query->orderBy('createdAt');
        }

        return $query;
    }

    public function index(Request $request)
    {
        $services = $this->services();
        $fuels = $this->fuels();
        $insurances = $this->insurances();
        $union = $services->unionAll($fuels)->unionAll($insurances);
        $query = DB::table(DB::raw("({$union->toSql()}) as data"));
        $results = $this->searchByName($request, $query);
        $results = $this->filterResults($request, $query);
        $results = $this->sortResults($request, $query);
        $results->paginate(10);
        $data["expenses"] = $results->get();

        return $data;
    }
}

