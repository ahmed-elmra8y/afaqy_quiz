<?php

use App\Http\Controllers\Apis\VehicleExpensesController;
use Illuminate\Support\Facades\Route;

Route::get('/vehicles_expenses', [VehicleExpensesController::class, 'index']);
